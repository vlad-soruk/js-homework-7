"use strict"

// Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який 
// міститиме будь-які дані, другий аргумент - тип даних

// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в 
// аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати 
// масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то 
// функція поверне масив [23, null].

function filterBy(arr, type) {
    let filteredArray = arr.filter( (element) => {return typeof(element) !== type} );
    return filteredArray;
}

let id = Symbol();
let user = {
    name: "Alex",
    surname: "Edison",
    age: 20,
};

let array = ['hello', 'world', undefined, 1234567890123456789012345678901234567890n,
                true, 23, '23', user, id, false, null];
console.log(array);

let type1 = "string";
let type2 = "number";
let type3 = "boolean";
let type4 = "object";
let type5 = "bigint";
let type6 = "symbol";
let type7 = "undefined";

let newArray = filterBy(array, type1);
console.log(newArray);

